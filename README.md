# Solution attempt for back-end task
Completed points 1, 2 and 3b.
## How to run
### Pre-requisites
- Java IDE with Gradle support
- Java 14

I used IntelliJ IDEA Community Edition 2020.2.2 x64  

### How I executed the program
- In IntelliJ IDEA on the right-hand side select **Gradle > restcountries > Tasks > application** and double-click **bootRun**
- Open a browser and go to any of the 3 end points:
  - http://localhost:8080/population
  - http://localhost:8080/area
  - http://localhost:8080/density

Alternatively:  
- In IntelliJ IDEA on the right-hand side select **Gradle > restcountries > Tasks > build** and double-click **bootJar**
- Using command line (for example, Windows PowerShell or Git Bash) got to _restcountries\build\libs_
- Execute by typing: **java -jar restcountries-0.0.1-SNAPSHOT.jar**
- Open a browser and go to any of the 3 end points:
  - http://localhost:8080/population
  - http://localhost:8080/area
  - http://localhost:8080/density

## Project information
- I have created the project through [Spring Initializr](https://start.spring.io/)
  - Project: **Gradle Project**
  - Spring Boot: **2.3.4**
  - Packaging: **Jar**
  - Java: **14**
  - Dependencies:
    - **Lombok**
    - **Spring Web**
- I have created the [countries_eu.json](src/main/resources/countries_eu.json) from [https://restcountries.eu](https://restcountries.eu)
  - I have used the following request: [https://restcountries.eu/rest/v2/regionalbloc/eu?fields=name;capital;currencies;population;area](https://restcountries.eu/rest/v2/regionalbloc/eu?fields=name;capital;currencies;population;area)
  - The [request](src/main/resources/restcountries_api_request.txt) is saved for reference in [src/main/resources](src/main/resources)
  - [countries_eu.json](src/main/resources/countries_eu.json) resides in [src/main/resources](src/main/resources)
- Source code resides in [src/main/java/lv/stone/restcountries](src/main/java/lv/stone/restcountries), or more specifically:
  - [dto](src/main/java/lv/stone/restcountries/dto)
  - [service](src/main/java/lv/stone/restcountries/service)
  - [controller](src/main/java/lv/stone/restcountries/controller)