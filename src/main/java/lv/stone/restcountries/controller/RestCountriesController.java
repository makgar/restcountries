package lv.stone.restcountries.controller;

import lv.stone.restcountries.dto.Country;
import lv.stone.restcountries.service.CountryService;
import lv.stone.restcountries.service.RestCountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class RestCountriesController {
    // Injecting CountryService bean
    @Autowired
    private CountryService countryService;
//    private CountryService countryService = new CountryService();
    @Autowired
    private RestCountryService restCountryService;

    @GetMapping("/population")
    public List<Country> getTopTenByPopulation() {
        return countryService.getTopTenByPopulation();
    }

    @GetMapping("/area")
    public List<Country> getTopTenByArea() {
        return countryService.getTopTenByArea();
    }

    @GetMapping("/density")
    public List<Country> getTopTenByPopulationDensity() {
        return countryService.getTopTenByPopulationDensity();
    }

//    @GetMapping("/test")
//    public List<Country> test() {
//        RestTemplate restTemplate = new RestTemplate();
//        ResponseEntity<List<Country>> response = restTemplate.exchange("https://restcountries.eu/rest/v2/regionalbloc/eu?fields=name;capital;currencies;population;area", HttpMethod.GET, null, new ParameterizedTypeReference<List<Country>>() {});
//        List<Country> result = response.getBody();
//        return result.stream()
//                .sorted(new Comparator<Country>() {
//                    @Override
//                    public int compare(Country country1, Country country2) {
//                        return country2.getPopulationDensity().compareTo(country1.getPopulationDensity());
//                    }
//                })
//                .limit(10)
//                .collect(Collectors.toList());
//    }

    @GetMapping("/density-rest")
    public List<Country> getTopTenByPopulationDensityRest() { return restCountryService.getTopTenByPopulationDensityRest(); }
}
