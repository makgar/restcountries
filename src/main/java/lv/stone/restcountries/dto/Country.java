// Data Transfer Object for Country

package lv.stone.restcountries.dto;

import lombok.Data;

import java.util.List;

// Lombok annotation that generates getters and setters
@Data
public class Country {
    /* According to https://restcountries.eu/ a Country object has a Currencies array which may have multiple Currency
       objects inside */
    private List<Currency> currencies;
    private String name;
    private String capital;
    private Long population;
    /* Setting default value of 0, because French Guiana retrieved from https://restcountries.eu/ does not have area
       field. I require area field value to calculate population density */
    private Float area = Float.valueOf(0);

//    public Country() {
//
//    }
//
//    public List<Currency> getCurrencies() {
//        return currencies;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public String getCapital() {
//        return capital;
//    }
//
//    public Long getPopulation() {
//        return population;
//    }
//
//    public Float getArea() {
//        return area;
//    }

    public Float getPopulationDensity() {
        // I'll only calculate density, if both population and area are provided, otherwise I'll default to 0
        if (population != null && area != null && area != 0) {
            return population / area;
        } else {
            return Float.valueOf(0);
        }
    }
}
