// Data Transfer Object for Currency

package lv.stone.restcountries.dto;

import lombok.Data;

// Lombok annotation that generates getters and setters
@Data
public class Currency {
    private String code;
    private String name;
    private String symbol;

//    public Currency() {
//
//    }
//
//    public String getCode() {
//        return code;
//    }
//
//    public void setCode(String codeValue) {
//        this.code = codeValue;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String nameValue) {
//        this.name = nameValue;
//    }
//
//    public String getSymbol() {
//        return symbol;
//    }
//
//    public void setSymbol(String symbolValue) {
//        this.symbol = symbolValue;
//    }
}
