package lv.stone.restcountries.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lv.stone.restcountries.dto.Country;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

// Marking bean as component
@Component
public class CountryService {

    // Method for retrieving all countries from JSON file
    private List<Country> getAllCountries() {
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            /* Using ClassPathResource and getInputStream in order for the file to be visible inside jar */
            return objectMapper.readValue(new ClassPathResource("countries_eu.json").getInputStream(), new TypeReference<List<Country>>() {});
        } catch (IOException e) {
            System.err.println("Could not parse input file.");
            e.printStackTrace();
            // Returning empty list, if something went wrong
            return Collections.emptyList();
        }
    }

    public List<Country> getTopTenByPopulation() {
        List<Country> allCountries = getAllCountries();
        return allCountries.stream() // Iterating through Country objects
                // Sorting by population
                .sorted(new Comparator<Country>() {
                    @Override
                    public int compare(Country country1, Country country2) {
                        return country2.getPopulation().compareTo(country1.getPopulation());
                    }
                })
                .limit(10) // Limiting result to 10 entries
                .collect(Collectors.toList()); // Terminating by making a List
    }

    public List<Country> getTopTenByArea() {
        List<Country> allCountries = getAllCountries();
        return allCountries.stream()
                .sorted(new Comparator<Country>() {
                    @Override
                    public int compare(Country country1, Country country2) {
                        return country2.getArea().compareTo(country1.getArea());
                    }
                })
                .limit(10)
                .collect(Collectors.toList());
    }

    public List<Country> getTopTenByPopulationDensity() {
        List<Country> allCountries = getAllCountries();
        return allCountries.stream()
                .sorted(new Comparator<Country>() {
                    @Override
                    public int compare(Country country1, Country country2) {
                        return country2.getPopulationDensity().compareTo(country1.getPopulationDensity());
                    }
                })
                .limit(10)
                .collect(Collectors.toList());
    }
}
