package lv.stone.restcountries.service;

import lv.stone.restcountries.dto.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RestCountryService {
    @Autowired
    private RestTemplate restTemplate;
    private String url = "https://restcountries.eu/rest/v2/regionalbloc/eu?fields=name;capital;currencies;population;area";

    private List<Country> getAllCountriesRest() {
        ResponseEntity<List<Country>> response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<List<Country>>() {});
        List<Country> result = response.getBody();
        return result;
    }

    // Method for retrieving data from REST
    public List<Country> getTopTenByPopulationDensityRest() {
        return getAllCountriesRest().stream()
                .sorted(new Comparator<Country>() {
                    @Override
                    public int compare(Country country1, Country country2) {
                        return country2.getPopulationDensity().compareTo(country1.getPopulationDensity());
                    }
                })
                .limit(10)
                .collect(Collectors.toList());
    }
}
